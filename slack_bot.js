if (!process.env.token) {
	console.log('Error: Specify token in environment');
	process.exit(1);
}

var cleverbot = require('cleverbot.io'),
	cleverbot = new cleverbot(process.env.CB_API_USER, process.env.CB_API_KEY);
cleverbot.setNick('Heinz');
cleverbot.create(function(err, session) {
	if (err) {
		console.log('cleverbot create fail.');
	} else {
		console.log('cleverbot create success.');
	}
});


var Botkit = require('botkit');
var os = require('os');
var request = require('request');

var controller = Botkit.slackbot({
	debug: true,
});

var bot = controller.spawn({
	token: process.env.token
}).startRTM();

/**
 * default listener if heinz doesn't understand the input
 */
controller.hears('', 'direct_message,direct_mention,mention,ambient', function(bot, message) {
	// bot.reply(message, 'Sorry I dont understand you! Tell me something about Chuck Norris, Heinz or Eier!');
	var msg = message.text;
	cleverbot.ask(msg, function(err, response) {
		if (!err) {
			bot.reply(message, response);
		} else {
			console.log('cleverbot err: ' + err);
			bot.reply(message, 'Sorry I dont understand you! Tell me something about Chuck Norris, Heinz or Eier!');
		}
	});
});

/**
 * listener for specific messages
 * makes a request to chuck norris api and replies a message
 */
controller.hears(['chuck norris', 'heinz', 'eier', 'eggs'], 'direct_message,direct_mention,mention,ambient', function(bot, message) {

	url = 'http://api.icndb.com/jokes/random';

	request(url, function(error, response, body) {
		if (!error) {
			var json = JSON.parse(body);
			bot.reply(message, json.value.joke);
		}
	});
});